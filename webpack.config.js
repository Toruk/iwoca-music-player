const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HOST = 8000;

module.exports = [
    {
        entry: ['babel-polyfill', 'webpack-dev-server/client?http://localhost:' + HOST,'./src/index.js', './src/scss/main.scss'],
        output: {
            path: path.resolve(__dirname, 'public'),
            publicPath: "/",
            filename: "bundle.js"
        },
        devtool: "source-map",
        module: {
            loaders: [
                { test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/ },
                {
                    test: /\.s?css$/,
                    exclude: /node_modules/,
                    use: ExtractTextPlugin.extract({
                        use: [{
                            loader: 'css-loader'
                        }, {
                            loader: 'postcss-loader',
                            options: {
                                plugins: (loader) => [
                                    require('autoprefixer')
                                ]
                            }
                        }, {
                            loader: 'sass-loader'
                        }],
                        fallback: "style-loader"
                    })
                },
            ]
        },
        devServer: {
            historyApiFallback: true,
            noInfo: true,
            port: HOST
        },
        plugins: [
            new ExtractTextPlugin("styles.css")
        ]
    }
];