import moment from 'moment';

export function formatTime(seconds) {
    return moment.utc(seconds*1000).format('mm:ss');
}