import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Sound extends Component {
    constructor(props) {
        super(props);
        this.audioElement = undefined;
    }

    componentDidMount() {
        this.audioElement = ReactDOM.findDOMNode(this.refs.audio);
        this.audioElement.addEventListener('timeupdate', this.handleTimeUpdate.bind(this), false);
        this.audioElement.pause();
    }

    componentDidUpdate() {
        const { isPlaying } = this.props;
        if (isPlaying) {
            this.audioElement.play();
        }else{
            this.audioElement.pause();
        }
    }

    handleTimeUpdate(e) {
        this.props.soundAction(this.audioElement.currentTime)
    }

    render() {
        return <audio id='audio' ref='audio' src={this.props.url}></audio>;
    }
}
export default Sound;