import React, { Component } from 'react';

import { formatTime } from './../../utils/format';

class PlayBar extends Component {
    render() {
        const { elapsed, duration } = this.props;
        let percent = (elapsed/duration) * 100;
        return (
            <div className="play-bar">
                <span className="play-bar__elapsed">{formatTime(elapsed)}</span>
                <div className="play-bar__bg">
                    <div className="play-bar__progress" style={{ width: `${percent}%` }}></div>
                </div>
                <span className="play-bar__total">{formatTime(duration)}</span>
            </div>
        )
    }
}
export default PlayBar;