import React, { Component } from 'react';

class Controls extends Component {

    handleEvent(e) {
        e.preventDefault();
        this.props.controlAction();
    }

    render() {
        const { isPlaying } = this.props;
        return (
            <div className="controls">
                <a href="#" className="controls__playpause"><i className={"fa fa-" + (isPlaying ? 'pause' : 'play')} onClick={this.handleEvent.bind(this)}></i></a>
            </div>
        )
    }
}
export default Controls;