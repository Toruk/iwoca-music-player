import React, { Component } from 'react';
// Child components
import Controls from './Controls';
import PlayBar from "./PlayBar";
import SongInfo from "./SongInfo";
import Sound from "./Sound";

class Player extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isPlaying: false,
            elapsed: 0,
            currentSong: undefined
        }
    }

    componentWillMount() {
        const { data } = this.props;
        this.setState({ currentSong: data });
    }

    componentWillReceiveProps(nextProps) {
        if (typeof nextProps.data != 'undefined') {
            this.setState({ currentSong: nextProps.data });
        }
    }

    handleControlEvent() {
        this.setState({
            isPlaying: !this.state.isPlaying,
        });
    }

    handleSoundEvent(elapsed) {
        this.setState({
            elapsed: elapsed
        })
    }

    render() {
        return (
            <div className="player">
                <PlayBar elapsed={this.state.elapsed} duration={this.state.currentSong.duration} />
                <div className="flex-container">
                    <SongInfo title={this.state.currentSong.title} artist={this.state.currentSong.artist} />
                    <Controls isPlaying={this.state.isPlaying} controlAction={this.handleControlEvent.bind(this)}/>
                    <Sound url={this.state.currentSong.streamUrl} isPlaying={this.state.isPlaying} soundAction={this.handleSoundEvent.bind(this)} />
                </div>
            </div>
        )
    }
}
export default Player;