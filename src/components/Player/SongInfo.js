import React, { Component } from 'react';

class SongInfo extends Component {
    render() {
        const { artist, title } = this.props;
        return (
            <div className="song-info">
                <span className="song-info__title">{title}</span>
                <span className="song-info__artist">{artist}</span>
            </div>
        )
    }
}
export default SongInfo;