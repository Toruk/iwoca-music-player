import React, { Component } from 'react';
import { formatTime } from './../../utils/format';

class SongItem extends Component {

    handleEvent(data) {
        const { action } = this.props;
        action(data);
    }

    render() {
        const { key, data } = this.props;
        return (
            <div className="song-item" key={key} onClick={this.handleEvent.bind(this, data)}>
                <div className="song-item__img-wrapper">
                    <img src={data.artworkUrl} alt=""/>
                </div>
                <div className="song-item__details">
                    <span className="song-item__title">{data.title}</span>
                    <span className="song-item__artist">{data.artist}</span>
                </div>
                <div className="song-item__duration">{formatTime(data.duration)}</div>
            </div>
        )
    }
}
export default SongItem;