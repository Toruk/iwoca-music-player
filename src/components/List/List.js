import React, { Component } from 'react';

import SongItem from './SongItem';

class List extends Component {

    handleSongEvent(data) {
        const { action } = this.props;
        action(data)
    }

    render() {
        const { songs } = this.props;
        return (
            <div className="list">
                <h1>My Library</h1>
                {songs.map((item, key) => {
                    return (
                        <SongItem key={key} data={item} action={this.handleSongEvent.bind(this)}/>
                    )
                })}
            </div>
        )
    }
}
export default List;