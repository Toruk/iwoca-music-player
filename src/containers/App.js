import React, { Component } from 'react';

// Import Components
import Player from '../components/Player/Player';
import List from "../components/List/List";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            songData: [
                {
                    title: 'The Prodigy - Breathe',
                    artist: 'Victor Ruiz',
                    duration: 226,
                    streamUrl: 'http://api.soundcloud.com/tracks/186800069/stream?client_id=11c11646280749c019f8e57a4a1f520c',
                    artworkUrl: 'https://i1.sndcdn.com/artworks-000103864555-l8j85i-large.jpg'
                },
                {
                    title: 'The Prodigy - Nasty (Spor Remix)',
                    artist: 'UKF',
                    duration: 309,
                    streamUrl: 'http://api.soundcloud.com/tracks/188326695/stream?client_id=11c11646280749c019f8e57a4a1f520c',
                    artworkUrl: 'https://i1.sndcdn.com/artworks-000104869725-xdwwmi-large.jpg'
                },
                {
                    title: 'Deep Enough - The Fast And Furious',
                    artist: 'Live',
                    duration: 190,
                    streamUrl: 'https://api.soundcloud.com/tracks/241363945/stream?client_id=11c11646280749c019f8e57a4a1f520c',
                    artworkUrl: 'https://i1.sndcdn.com/artworks-000142669285-gcdrh1-large.jpg'
                },
                {
                    title: 'Fast And Furious - My life be like',
                    artist: 'Steve S',
                    duration: 233,
                    streamUrl: 'https://api.soundcloud.com/tracks/300223192/stream?client_id=11c11646280749c019f8e57a4a1f520c',
                    artworkUrl: 'https://i1.sndcdn.com/artworks-000200613627-7b6j15-large.jpg'
                }
            ],
            currentSong: {
                title: 'The Prodigy - Breathe',
                artist: 'Victor Ruiz',
                duration: 226,
                streamUrl: 'http://api.soundcloud.com/tracks/186800069/stream?client_id=11c11646280749c019f8e57a4a1f520c',
                artworkUrl: 'https://i1.sndcdn.com/artworks-000103864555-l8j85i-large.jpg'
            }
        }
    }

    handler(data) {
        this.setState({ currentSong: data });
    }

    render() {
        return (
           <main className="app">
               <List songs={this.state.songData} action={this.handler.bind(this)}/>
               <Player data={this.state.currentSong}/>
           </main>
        )
    }
}
export default App