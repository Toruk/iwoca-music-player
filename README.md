## Tech Stack ##
React
Sass
Webpack

## Setup ##
To install please run the following commands in terminal

You will require Node in order to run this web app

- `npm install`
- `npm start` - should open the app in default browser


## Notes ##
If I had enough time. I would have used redux, which would have been more appropriate for a project like this.

Also sorry for bad design not much of a designer.